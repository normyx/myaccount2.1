package org.mgoulene.repository;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.mgoulene.domain.AccountGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class AccountGroupRepositoryWithBagRelationshipsImpl implements AccountGroupRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<AccountGroup> fetchBagRelationships(Optional<AccountGroup> accountGroup) {
        return accountGroup.map(this::fetchBankAccounts);
    }

    @Override
    public Page<AccountGroup> fetchBagRelationships(Page<AccountGroup> accountGroups) {
        return new PageImpl<>(
            fetchBagRelationships(accountGroups.getContent()),
            accountGroups.getPageable(),
            accountGroups.getTotalElements()
        );
    }

    @Override
    public List<AccountGroup> fetchBagRelationships(List<AccountGroup> accountGroups) {
        return Optional.of(accountGroups).map(this::fetchBankAccounts).orElse(Collections.emptyList());
    }

    AccountGroup fetchBankAccounts(AccountGroup result) {
        return entityManager
            .createQuery(
                "select accountGroup from AccountGroup accountGroup left join fetch accountGroup.bankAccounts where accountGroup is :accountGroup",
                AccountGroup.class
            )
            .setParameter("accountGroup", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<AccountGroup> fetchBankAccounts(List<AccountGroup> accountGroups) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, accountGroups.size()).forEach(index -> order.put(accountGroups.get(index).getId(), index));
        List<AccountGroup> result = entityManager
            .createQuery(
                "select distinct accountGroup from AccountGroup accountGroup left join fetch accountGroup.bankAccounts where accountGroup in :accountGroups",
                AccountGroup.class
            )
            .setParameter("accountGroups", accountGroups)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
