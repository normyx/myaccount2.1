package org.mgoulene.repository;

import java.util.List;
import java.util.Optional;
import org.mgoulene.domain.AccountGroup;
import org.springframework.data.domain.Page;

public interface AccountGroupRepositoryWithBagRelationships {
    Optional<AccountGroup> fetchBagRelationships(Optional<AccountGroup> accountGroup);

    List<AccountGroup> fetchBagRelationships(List<AccountGroup> accountGroups);

    Page<AccountGroup> fetchBagRelationships(Page<AccountGroup> accountGroups);
}
