package org.mgoulene.repository;

import java.util.List;
import java.util.Optional;
import org.mgoulene.domain.AccountGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AccountGroup entity.
 *
 * When extending this class, extend AccountGroupRepositoryWithBagRelationships too.
 * For more information refer to https://github.com/jhipster/generator-jhipster/issues/17990.
 */
@Repository
public interface AccountGroupRepository
    extends AccountGroupRepositoryWithBagRelationships, JpaRepository<AccountGroup, Long>, JpaSpecificationExecutor<AccountGroup> {
    default Optional<AccountGroup> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findOneWithToOneRelationships(id));
    }

    default List<AccountGroup> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAllWithToOneRelationships());
    }

    default Page<AccountGroup> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAllWithToOneRelationships(pageable));
    }

    @Query(
        value = "select distinct accountGroup from AccountGroup accountGroup left join fetch accountGroup.account",
        countQuery = "select count(distinct accountGroup) from AccountGroup accountGroup"
    )
    Page<AccountGroup> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct accountGroup from AccountGroup accountGroup left join fetch accountGroup.account")
    List<AccountGroup> findAllWithToOneRelationships();

    @Query("select accountGroup from AccountGroup accountGroup left join fetch accountGroup.account where accountGroup.id =:id")
    Optional<AccountGroup> findOneWithToOneRelationships(@Param("id") Long id);
}
