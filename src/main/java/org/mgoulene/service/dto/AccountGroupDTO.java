package org.mgoulene.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link org.mgoulene.domain.AccountGroup} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AccountGroupDTO implements Serializable {

    private Long id;

    @NotNull
    private String groupName;

    @NotNull
    private Boolean defaultActive;

    private ApplicationUserDTO account;

    private Set<BankAccountDTO> bankAccounts = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Boolean getDefaultActive() {
        return defaultActive;
    }

    public void setDefaultActive(Boolean defaultActive) {
        this.defaultActive = defaultActive;
    }

    public ApplicationUserDTO getAccount() {
        return account;
    }

    public void setAccount(ApplicationUserDTO account) {
        this.account = account;
    }

    public Set<BankAccountDTO> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(Set<BankAccountDTO> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccountGroupDTO)) {
            return false;
        }

        AccountGroupDTO accountGroupDTO = (AccountGroupDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, accountGroupDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AccountGroupDTO{" +
            "id=" + getId() +
            ", groupName='" + getGroupName() + "'" +
            ", defaultActive='" + getDefaultActive() + "'" +
            ", account=" + getAccount() +
            ", bankAccounts=" + getBankAccounts() +
            "}";
    }
}
