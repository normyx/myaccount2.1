package org.mgoulene.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import org.mgoulene.domain.AccountGroup;
import org.mgoulene.domain.ApplicationUser;
import org.mgoulene.domain.BankAccount;
import org.mgoulene.service.dto.AccountGroupDTO;
import org.mgoulene.service.dto.ApplicationUserDTO;
import org.mgoulene.service.dto.BankAccountDTO;

/**
 * Mapper for the entity {@link AccountGroup} and its DTO {@link AccountGroupDTO}.
 */
@Mapper(componentModel = "spring")
public interface AccountGroupMapper extends EntityMapper<AccountGroupDTO, AccountGroup> {
    @Mapping(target = "account", source = "account", qualifiedByName = "applicationUserNickName")
    @Mapping(target = "bankAccounts", source = "bankAccounts", qualifiedByName = "bankAccountAccountNameSet")
    AccountGroupDTO toDto(AccountGroup s);

    @Mapping(target = "removeBankAccount", ignore = true)
    AccountGroup toEntity(AccountGroupDTO accountGroupDTO);

    @Named("applicationUserNickName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "nickName", source = "nickName")
    ApplicationUserDTO toDtoApplicationUserNickName(ApplicationUser applicationUser);

    @Named("bankAccountAccountName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "accountName", source = "accountName")
    BankAccountDTO toDtoBankAccountAccountName(BankAccount bankAccount);

    @Named("bankAccountAccountNameSet")
    default Set<BankAccountDTO> toDtoBankAccountAccountNameSet(Set<BankAccount> bankAccount) {
        return bankAccount.stream().map(this::toDtoBankAccountAccountName).collect(Collectors.toSet());
    }
}
