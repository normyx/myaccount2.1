package org.mgoulene.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A AccountGroup.
 */
@Entity
@Table(name = "account_group")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "accountgroup")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AccountGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "group_name", nullable = false)
    private String groupName;

    @NotNull
    @Column(name = "default_active", nullable = false)
    private Boolean defaultActive;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "user" }, allowSetters = true)
    private ApplicationUser account;

    @ManyToMany
    @JoinTable(
        name = "rel_account_group__bank_account",
        joinColumns = @JoinColumn(name = "account_group_id"),
        inverseJoinColumns = @JoinColumn(name = "bank_account_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "account", "stockPortfolioItems", "realEstateItems", "accountGroups" }, allowSetters = true)
    private Set<BankAccount> bankAccounts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public AccountGroup id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public AccountGroup groupName(String groupName) {
        this.setGroupName(groupName);
        return this;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Boolean getDefaultActive() {
        return this.defaultActive;
    }

    public AccountGroup defaultActive(Boolean defaultActive) {
        this.setDefaultActive(defaultActive);
        return this;
    }

    public void setDefaultActive(Boolean defaultActive) {
        this.defaultActive = defaultActive;
    }

    public ApplicationUser getAccount() {
        return this.account;
    }

    public void setAccount(ApplicationUser applicationUser) {
        this.account = applicationUser;
    }

    public AccountGroup account(ApplicationUser applicationUser) {
        this.setAccount(applicationUser);
        return this;
    }

    public Set<BankAccount> getBankAccounts() {
        return this.bankAccounts;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public AccountGroup bankAccounts(Set<BankAccount> bankAccounts) {
        this.setBankAccounts(bankAccounts);
        return this;
    }

    public AccountGroup addBankAccount(BankAccount bankAccount) {
        this.bankAccounts.add(bankAccount);
        bankAccount.getAccountGroups().add(this);
        return this;
    }

    public AccountGroup removeBankAccount(BankAccount bankAccount) {
        this.bankAccounts.remove(bankAccount);
        bankAccount.getAccountGroups().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccountGroup)) {
            return false;
        }
        return id != null && id.equals(((AccountGroup) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AccountGroup{" +
            "id=" + getId() +
            ", groupName='" + getGroupName() + "'" +
            ", defaultActive='" + getDefaultActive() + "'" +
            "}";
    }
}
