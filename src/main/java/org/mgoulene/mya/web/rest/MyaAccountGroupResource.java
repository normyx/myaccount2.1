package org.mgoulene.mya.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.mgoulene.mya.service.MyaApplicationUserService;
import org.mgoulene.repository.AccountGroupRepository;
import org.mgoulene.service.AccountGroupQueryService;
import org.mgoulene.service.AccountGroupService;
import org.mgoulene.service.criteria.AccountGroupCriteria;
import org.mgoulene.service.dto.AccountGroupDTO;
import org.mgoulene.service.dto.ApplicationUserDTO;
import org.mgoulene.service.dto.BankAccountDTO;
import org.mgoulene.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link org.mgoulene.domain.AccountGroup}.
 */
@RestController
@RequestMapping("/api")
public class MyaAccountGroupResource {

    private final Logger log = LoggerFactory.getLogger(MyaAccountGroupResource.class);

    private static final String ENTITY_NAME = "accountGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AccountGroupService accountGroupService;

    private final AccountGroupRepository accountGroupRepository;

    private final AccountGroupQueryService accountGroupQueryService;

    private final MyaApplicationUserService myaApplicationUserService;

    public MyaAccountGroupResource(
        AccountGroupService accountGroupService,
        AccountGroupRepository accountGroupRepository,
        AccountGroupQueryService accountGroupQueryService,
        MyaApplicationUserService myaApplicationUserService
    ) {
        this.accountGroupService = accountGroupService;
        this.accountGroupRepository = accountGroupRepository;
        this.accountGroupQueryService = accountGroupQueryService;
        this.myaApplicationUserService = myaApplicationUserService;
    }

    /**
     * {@code GET  /account-groups} : get all the accountGroups.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of accountGroups in body.
     */
    @GetMapping("/mya/account-groups/with-signedin-user")
    public ResponseEntity<List<AccountGroupDTO>> getAllAccountGroups(AccountGroupCriteria criteria) {
        log.debug("REST request to get AccountGroups by criteria and logged in user: {}", criteria);
        Optional<ApplicationUserDTO> applicationUserOptional = myaApplicationUserService.findSignedInApplicationUser();
        if (applicationUserOptional.isPresent()) {
            LongFilter userFilter = new LongFilter();
            userFilter.setEquals(applicationUserOptional.get().getId());
            criteria.setAccountId(userFilter);
            List<AccountGroupDTO> entityList = accountGroupQueryService.findByCriteria(criteria);
            return ResponseEntity.ok().body(entityList);
        }
        return null;
    }
}
