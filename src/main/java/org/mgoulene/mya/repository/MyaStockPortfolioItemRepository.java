package org.mgoulene.mya.repository;

import java.util.List;
import org.mgoulene.domain.StockPortfolioItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the StockPortfolioItem entity.
 */
@Repository
public interface MyaStockPortfolioItemRepository
    extends JpaRepository<StockPortfolioItem, Long>, JpaSpecificationExecutor<StockPortfolioItem> {
    @Query(
        "select distinct stockPortfolioItem from StockPortfolioItem stockPortfolioItem where stockPortfolioItem.stockSymbol = :symbol and stockPortfolioItem.bankAccount.account.id = :applicationId"
    )
    List<StockPortfolioItem> findAllWithSymbolAndApplicationUser(
        @Param("symbol") String symbol,
        @Param("applicationId") Long applicationId
    );

    @Query(
        "select distinct stockPortfolioItem from StockPortfolioItem stockPortfolioItem where stockPortfolioItem.bankAccount in (select distinct bankAccount from AccountGroup account_group left join account_group.bankAccounts bankAccount  where account_group.id in :accountGroupIds) "
    )
    List<StockPortfolioItem> findAllInAccountGroupIds(@Param("accountGroupIds") List<Long> accountGroupIds);

    @Query(
        "select distinct stockPortfolioItem from StockPortfolioItem stockPortfolioItem  where stockPortfolioItem.bankAccount.account.id = :applicationId"
    )
    List<StockPortfolioItem> findAllWithApplicationUser(@Param("applicationId") Long applicationId);
}
