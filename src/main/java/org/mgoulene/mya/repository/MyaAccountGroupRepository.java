package org.mgoulene.mya.repository;

import java.util.List;
import org.mgoulene.domain.AccountGroup;
import org.mgoulene.repository.AccountGroupRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AccountGroup entity.
 *
 * When extending this class, extend AccountGroupRepositoryWithBagRelationships too.
 * For more information refer to https://github.com/jhipster/generator-jhipster/issues/17990.
 */
@Repository
public interface MyaAccountGroupRepository extends JpaRepository<AccountGroup, Long>, JpaSpecificationExecutor<AccountGroup> {
    @Query("select distinct accountGroup from AccountGroup accountGroup where accountGroup.id in :accountGroupIds")
    List<AccountGroup> findAllWithinAccountGroups(@Param("accountGroupIds") List<Long> accountGroupIds);
}
