
select 
	op_date,
	sum(amount) over (
order by
	op_date) + sum(initial_amount) over (
order by
	op_date) as amount,
	CAST(NULL AS SIGNED)
from
	(
	select
		date as op_date,
		SUM(amount) as amount,
		ba_data.initial_amount
	from
		operation op
	inner join bank_account ba on
		op.bank_account_id = ba.id
	left join (
		select
			ba.id as ba_id, 
			min(date) as op_date2,
			ba.initial_amount  as initial_amount
		from
			operation op
		inner join bank_account ba on
			op.bank_account_id = ba.id
		where
			ba.account_id = :accountId
			AND (ba.account_type = 'CURRENTACCOUNT')
			and ba.id in (select distinct ragba.bank_account_id from rel_account_group__bank_account ragba where ragba.account_group_id in :accountGroupIds)
		GROUP by
			ba.id) as ba_data on
		op.date = ba_data.op_date2
		AND op.bank_account_id = ba_data.ba_id
	where
		ba.account_id = :accountId
		AND (ba.account_type = 'CURRENTACCOUNT')
		and ba.id in (select distinct ragba.bank_account_id from rel_account_group__bank_account ragba where ragba.account_group_id in :accountGroupIds)
	group by
		op_date, ba_data.initial_amount
) as day_op