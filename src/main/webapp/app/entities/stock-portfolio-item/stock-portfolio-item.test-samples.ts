import dayjs from 'dayjs/esm';

import { Currency } from 'app/entities/enumerations/currency.model';
import { StockType } from 'app/entities/enumerations/stock-type.model';

import { IStockPortfolioItem, NewStockPortfolioItem } from './stock-portfolio-item.model';

export const sampleWithRequiredData: IStockPortfolioItem = {
  id: 16345,
  stockSymbol: 'hack b inn',
  stockCurrency: Currency['EUR'],
  stockAcquisitionDate: dayjs('2022-11-06'),
  stockSharesNumber: 88040,
  stockAcquisitionPrice: 23686,
  stockCurrentPrice: 58399,
  stockCurrentDate: dayjs('2022-11-06'),
  stockAcquisitionCurrencyFactor: 21249,
  stockCurrentCurrencyFactor: 57422,
  stockPriceAtAcquisitionDate: 49639,
  stockType: StockType['CRYPTO'],
};

export const sampleWithPartialData: IStockPortfolioItem = {
  id: 90041,
  stockSymbol: 'methodolog',
  stockCurrency: Currency['GBP'],
  stockAcquisitionDate: dayjs('2022-11-06'),
  stockSharesNumber: 87919,
  stockAcquisitionPrice: 9741,
  stockCurrentPrice: 1564,
  stockCurrentDate: dayjs('2022-11-06'),
  stockAcquisitionCurrencyFactor: 60647,
  stockCurrentCurrencyFactor: 13101,
  stockPriceAtAcquisitionDate: 21794,
  stockType: StockType['CRYPTO'],
  stockSellCurrencyFactor: 62871,
};

export const sampleWithFullData: IStockPortfolioItem = {
  id: 61696,
  stockSymbol: 'models',
  stockCurrency: Currency['EUR'],
  stockAcquisitionDate: dayjs('2022-11-05'),
  stockSharesNumber: 92338,
  stockAcquisitionPrice: 13773,
  stockCurrentPrice: 1170,
  stockCurrentDate: dayjs('2022-11-06'),
  stockAcquisitionCurrencyFactor: 2471,
  stockCurrentCurrencyFactor: 67341,
  stockPriceAtAcquisitionDate: 83868,
  stockType: StockType['STOCK'],
  lastStockUpdate: dayjs('2022-11-06T00:44'),
  lastCurrencyUpdate: dayjs('2022-11-06T00:15'),
  stockSellDate: dayjs('2022-11-06'),
  stockSellPrice: 61988,
  stockSellCurrencyFactor: 83435,
};

export const sampleWithNewData: NewStockPortfolioItem = {
  stockSymbol: 'Integrated',
  stockCurrency: Currency['EUR'],
  stockAcquisitionDate: dayjs('2022-11-06'),
  stockSharesNumber: 1757,
  stockAcquisitionPrice: 33364,
  stockCurrentPrice: 63345,
  stockCurrentDate: dayjs('2022-11-05'),
  stockAcquisitionCurrencyFactor: 7481,
  stockCurrentCurrencyFactor: 59648,
  stockPriceAtAcquisitionDate: 56004,
  stockType: StockType['STOCK'],
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
