import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../account-group.test-samples';

import { AccountGroupFormService } from './account-group-form.service';

describe('AccountGroup Form Service', () => {
  let service: AccountGroupFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountGroupFormService);
  });

  describe('Service methods', () => {
    describe('createAccountGroupFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createAccountGroupFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            groupName: expect.any(Object),
            defaultActive: expect.any(Object),
            account: expect.any(Object),
            bankAccounts: expect.any(Object),
          })
        );
      });

      it('passing IAccountGroup should create a new form with FormGroup', () => {
        const formGroup = service.createAccountGroupFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            groupName: expect.any(Object),
            defaultActive: expect.any(Object),
            account: expect.any(Object),
            bankAccounts: expect.any(Object),
          })
        );
      });
    });

    describe('getAccountGroup', () => {
      it('should return NewAccountGroup for default AccountGroup initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createAccountGroupFormGroup(sampleWithNewData);

        const accountGroup = service.getAccountGroup(formGroup) as any;

        expect(accountGroup).toMatchObject(sampleWithNewData);
      });

      it('should return NewAccountGroup for empty AccountGroup initial value', () => {
        const formGroup = service.createAccountGroupFormGroup();

        const accountGroup = service.getAccountGroup(formGroup) as any;

        expect(accountGroup).toMatchObject({});
      });

      it('should return IAccountGroup', () => {
        const formGroup = service.createAccountGroupFormGroup(sampleWithRequiredData);

        const accountGroup = service.getAccountGroup(formGroup) as any;

        expect(accountGroup).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IAccountGroup should not enable id FormControl', () => {
        const formGroup = service.createAccountGroupFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewAccountGroup should disable id FormControl', () => {
        const formGroup = service.createAccountGroupFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
