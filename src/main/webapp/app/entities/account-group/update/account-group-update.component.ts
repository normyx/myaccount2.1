import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { AccountGroupFormService, AccountGroupFormGroup } from './account-group-form.service';
import { IAccountGroup } from '../account-group.model';
import { AccountGroupService } from '../service/account-group.service';
import { IApplicationUser } from 'app/entities/application-user/application-user.model';
import { ApplicationUserService } from 'app/entities/application-user/service/application-user.service';
import { IBankAccount } from 'app/entities/bank-account/bank-account.model';
import { BankAccountService } from 'app/entities/bank-account/service/bank-account.service';

@Component({
  selector: 'jhi-account-group-update',
  templateUrl: './account-group-update.component.html',
})
export class AccountGroupUpdateComponent implements OnInit {
  isSaving = false;
  accountGroup: IAccountGroup | null = null;

  applicationUsersSharedCollection: IApplicationUser[] = [];
  bankAccountsSharedCollection: IBankAccount[] = [];

  editForm: AccountGroupFormGroup = this.accountGroupFormService.createAccountGroupFormGroup();

  constructor(
    protected accountGroupService: AccountGroupService,
    protected accountGroupFormService: AccountGroupFormService,
    protected applicationUserService: ApplicationUserService,
    protected bankAccountService: BankAccountService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareApplicationUser = (o1: IApplicationUser | null, o2: IApplicationUser | null): boolean =>
    this.applicationUserService.compareApplicationUser(o1, o2);

  compareBankAccount = (o1: IBankAccount | null, o2: IBankAccount | null): boolean => this.bankAccountService.compareBankAccount(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accountGroup }) => {
      this.accountGroup = accountGroup;
      if (accountGroup) {
        this.updateForm(accountGroup);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const accountGroup = this.accountGroupFormService.getAccountGroup(this.editForm);
    if (accountGroup.id !== null) {
      this.subscribeToSaveResponse(this.accountGroupService.update(accountGroup));
    } else {
      this.subscribeToSaveResponse(this.accountGroupService.create(accountGroup));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAccountGroup>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(accountGroup: IAccountGroup): void {
    this.accountGroup = accountGroup;
    this.accountGroupFormService.resetForm(this.editForm, accountGroup);

    this.applicationUsersSharedCollection = this.applicationUserService.addApplicationUserToCollectionIfMissing<IApplicationUser>(
      this.applicationUsersSharedCollection,
      accountGroup.account
    );
    this.bankAccountsSharedCollection = this.bankAccountService.addBankAccountToCollectionIfMissing<IBankAccount>(
      this.bankAccountsSharedCollection,
      ...(accountGroup.bankAccounts ?? [])
    );
  }

  protected loadRelationshipsOptions(): void {
    this.applicationUserService
      .query()
      .pipe(map((res: HttpResponse<IApplicationUser[]>) => res.body ?? []))
      .pipe(
        map((applicationUsers: IApplicationUser[]) =>
          this.applicationUserService.addApplicationUserToCollectionIfMissing<IApplicationUser>(
            applicationUsers,
            this.accountGroup?.account
          )
        )
      )
      .subscribe((applicationUsers: IApplicationUser[]) => (this.applicationUsersSharedCollection = applicationUsers));

    this.bankAccountService
      .query()
      .pipe(map((res: HttpResponse<IBankAccount[]>) => res.body ?? []))
      .pipe(
        map((bankAccounts: IBankAccount[]) =>
          this.bankAccountService.addBankAccountToCollectionIfMissing<IBankAccount>(
            bankAccounts,
            ...(this.accountGroup?.bankAccounts ?? [])
          )
        )
      )
      .subscribe((bankAccounts: IBankAccount[]) => (this.bankAccountsSharedCollection = bankAccounts));
  }
}
