import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { AccountGroupFormService } from './account-group-form.service';
import { AccountGroupService } from '../service/account-group.service';
import { IAccountGroup } from '../account-group.model';
import { IApplicationUser } from 'app/entities/application-user/application-user.model';
import { ApplicationUserService } from 'app/entities/application-user/service/application-user.service';
import { IBankAccount } from 'app/entities/bank-account/bank-account.model';
import { BankAccountService } from 'app/entities/bank-account/service/bank-account.service';

import { AccountGroupUpdateComponent } from './account-group-update.component';

describe('AccountGroup Management Update Component', () => {
  let comp: AccountGroupUpdateComponent;
  let fixture: ComponentFixture<AccountGroupUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let accountGroupFormService: AccountGroupFormService;
  let accountGroupService: AccountGroupService;
  let applicationUserService: ApplicationUserService;
  let bankAccountService: BankAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [AccountGroupUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AccountGroupUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AccountGroupUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    accountGroupFormService = TestBed.inject(AccountGroupFormService);
    accountGroupService = TestBed.inject(AccountGroupService);
    applicationUserService = TestBed.inject(ApplicationUserService);
    bankAccountService = TestBed.inject(BankAccountService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call ApplicationUser query and add missing value', () => {
      const accountGroup: IAccountGroup = { id: 456 };
      const account: IApplicationUser = { id: 99125 };
      accountGroup.account = account;

      const applicationUserCollection: IApplicationUser[] = [{ id: 95696 }];
      jest.spyOn(applicationUserService, 'query').mockReturnValue(of(new HttpResponse({ body: applicationUserCollection })));
      const additionalApplicationUsers = [account];
      const expectedCollection: IApplicationUser[] = [...additionalApplicationUsers, ...applicationUserCollection];
      jest.spyOn(applicationUserService, 'addApplicationUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ accountGroup });
      comp.ngOnInit();

      expect(applicationUserService.query).toHaveBeenCalled();
      expect(applicationUserService.addApplicationUserToCollectionIfMissing).toHaveBeenCalledWith(
        applicationUserCollection,
        ...additionalApplicationUsers.map(expect.objectContaining)
      );
      expect(comp.applicationUsersSharedCollection).toEqual(expectedCollection);
    });

    it('Should call BankAccount query and add missing value', () => {
      const accountGroup: IAccountGroup = { id: 456 };
      const bankAccounts: IBankAccount[] = [{ id: 77808 }];
      accountGroup.bankAccounts = bankAccounts;

      const bankAccountCollection: IBankAccount[] = [{ id: 13952 }];
      jest.spyOn(bankAccountService, 'query').mockReturnValue(of(new HttpResponse({ body: bankAccountCollection })));
      const additionalBankAccounts = [...bankAccounts];
      const expectedCollection: IBankAccount[] = [...additionalBankAccounts, ...bankAccountCollection];
      jest.spyOn(bankAccountService, 'addBankAccountToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ accountGroup });
      comp.ngOnInit();

      expect(bankAccountService.query).toHaveBeenCalled();
      expect(bankAccountService.addBankAccountToCollectionIfMissing).toHaveBeenCalledWith(
        bankAccountCollection,
        ...additionalBankAccounts.map(expect.objectContaining)
      );
      expect(comp.bankAccountsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const accountGroup: IAccountGroup = { id: 456 };
      const account: IApplicationUser = { id: 18635 };
      accountGroup.account = account;
      const bankAccount: IBankAccount = { id: 89201 };
      accountGroup.bankAccounts = [bankAccount];

      activatedRoute.data = of({ accountGroup });
      comp.ngOnInit();

      expect(comp.applicationUsersSharedCollection).toContain(account);
      expect(comp.bankAccountsSharedCollection).toContain(bankAccount);
      expect(comp.accountGroup).toEqual(accountGroup);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAccountGroup>>();
      const accountGroup = { id: 123 };
      jest.spyOn(accountGroupFormService, 'getAccountGroup').mockReturnValue(accountGroup);
      jest.spyOn(accountGroupService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ accountGroup });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: accountGroup }));
      saveSubject.complete();

      // THEN
      expect(accountGroupFormService.getAccountGroup).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(accountGroupService.update).toHaveBeenCalledWith(expect.objectContaining(accountGroup));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAccountGroup>>();
      const accountGroup = { id: 123 };
      jest.spyOn(accountGroupFormService, 'getAccountGroup').mockReturnValue({ id: null });
      jest.spyOn(accountGroupService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ accountGroup: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: accountGroup }));
      saveSubject.complete();

      // THEN
      expect(accountGroupFormService.getAccountGroup).toHaveBeenCalled();
      expect(accountGroupService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAccountGroup>>();
      const accountGroup = { id: 123 };
      jest.spyOn(accountGroupService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ accountGroup });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(accountGroupService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareApplicationUser', () => {
      it('Should forward to applicationUserService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(applicationUserService, 'compareApplicationUser');
        comp.compareApplicationUser(entity, entity2);
        expect(applicationUserService.compareApplicationUser).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareBankAccount', () => {
      it('Should forward to bankAccountService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(bankAccountService, 'compareBankAccount');
        comp.compareBankAccount(entity, entity2);
        expect(bankAccountService.compareBankAccount).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
