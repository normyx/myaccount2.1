import { IAccountGroup, NewAccountGroup } from './account-group.model';

export const sampleWithRequiredData: IAccountGroup = {
  id: 79217,
  groupName: 'Île-de-France',
  defaultActive: false,
};

export const sampleWithPartialData: IAccountGroup = {
  id: 62506,
  groupName: 'even-keeled',
  defaultActive: false,
};

export const sampleWithFullData: IAccountGroup = {
  id: 41,
  groupName: 'synthesize',
  defaultActive: true,
};

export const sampleWithNewData: NewAccountGroup = {
  groupName: 'Soap',
  defaultActive: false,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
