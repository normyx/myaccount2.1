import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AccountGroupComponent } from './list/account-group.component';
import { AccountGroupDetailComponent } from './detail/account-group-detail.component';
import { AccountGroupUpdateComponent } from './update/account-group-update.component';
import { AccountGroupDeleteDialogComponent } from './delete/account-group-delete-dialog.component';
import { AccountGroupRoutingModule } from './route/account-group-routing.module';

@NgModule({
  imports: [SharedModule, AccountGroupRoutingModule],
  declarations: [AccountGroupComponent, AccountGroupDetailComponent, AccountGroupUpdateComponent, AccountGroupDeleteDialogComponent],
})
export class AccountGroupModule {}
