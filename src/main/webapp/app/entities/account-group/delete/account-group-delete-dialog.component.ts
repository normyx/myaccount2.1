import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAccountGroup } from '../account-group.model';
import { AccountGroupService } from '../service/account-group.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './account-group-delete-dialog.component.html',
})
export class AccountGroupDeleteDialogComponent {
  accountGroup?: IAccountGroup;

  constructor(protected accountGroupService: AccountGroupService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.accountGroupService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
