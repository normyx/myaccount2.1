import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAccountGroup } from '../account-group.model';
import { AccountGroupService } from '../service/account-group.service';

@Injectable({ providedIn: 'root' })
export class AccountGroupRoutingResolveService implements Resolve<IAccountGroup | null> {
  constructor(protected service: AccountGroupService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAccountGroup | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((accountGroup: HttpResponse<IAccountGroup>) => {
          if (accountGroup.body) {
            return of(accountGroup.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
