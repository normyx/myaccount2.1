import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AccountGroupComponent } from '../list/account-group.component';
import { AccountGroupDetailComponent } from '../detail/account-group-detail.component';
import { AccountGroupUpdateComponent } from '../update/account-group-update.component';
import { AccountGroupRoutingResolveService } from './account-group-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const accountGroupRoute: Routes = [
  {
    path: '',
    component: AccountGroupComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AccountGroupDetailComponent,
    resolve: {
      accountGroup: AccountGroupRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AccountGroupUpdateComponent,
    resolve: {
      accountGroup: AccountGroupRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AccountGroupUpdateComponent,
    resolve: {
      accountGroup: AccountGroupRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(accountGroupRoute)],
  exports: [RouterModule],
})
export class AccountGroupRoutingModule {}
