import { IApplicationUser } from 'app/entities/application-user/application-user.model';
import { IBankAccount } from 'app/entities/bank-account/bank-account.model';

export interface IAccountGroup {
  id: number;
  groupName?: string | null;
  defaultActive?: boolean | null;
  account?: Pick<IApplicationUser, 'id' | 'nickName'> | null;
  bankAccounts?: Pick<IBankAccount, 'id' | 'accountName'>[] | null;
}

export type NewAccountGroup = Omit<IAccountGroup, 'id'> & { id: null };
