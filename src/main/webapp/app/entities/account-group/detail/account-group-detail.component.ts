import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAccountGroup } from '../account-group.model';

@Component({
  selector: 'jhi-account-group-detail',
  templateUrl: './account-group-detail.component.html',
})
export class AccountGroupDetailComponent implements OnInit {
  accountGroup: IAccountGroup | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accountGroup }) => {
      this.accountGroup = accountGroup;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
