import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AccountGroupDetailComponent } from './account-group-detail.component';

describe('AccountGroup Management Detail Component', () => {
  let comp: AccountGroupDetailComponent;
  let fixture: ComponentFixture<AccountGroupDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccountGroupDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ accountGroup: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AccountGroupDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AccountGroupDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load accountGroup on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.accountGroup).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
