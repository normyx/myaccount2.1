import { HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IOperation } from 'app/entities/operation/operation.model';
import { EVENT_LOAD_BUDGET_ITEMS } from 'app/mya/config/mya.event.constants';
import dayjs, { Dayjs } from 'dayjs';
import { EventManager } from '../../../core/util/event-manager.service';
import { IBudgetItemPeriod } from '../../../entities/budget-item-period/budget-item-period.model';
import { IBudgetItem } from '../../../entities/budget-item/budget-item.model';
import { MyaOperationService } from '../../mya-operation/service/mya-operation.service';
import { MyaBudgetItemPeriodDeleteDialogComponent } from '../delete/mya-budget-item-period-delete-dialog.component';
import { MyaBudgetItemPeriodService } from '../service/mya-budget-item-period.service';
import { MyaBudgetItemPeriodUpdateDialogComponent } from '../update/mya-budget-item-period-update-dialog.component';

@Component({
  selector: '[mya-budget-item-period-cell-element]', // eslint-disable-line no-use-before-define
  templateUrl: './mya-budget-item-period-cell-element.component.html',
  styleUrls: ['./mya-budget-item-period-cell.component.scss'],
})
export class MyaBudgetItemPeriodCellElementComponent implements OnInit {
  @Input() budgetItemPeriod: IBudgetItemPeriod | null = null;
  @Input() budgetItem: IBudgetItem | null = null;
  @Input() editable: boolean | null = null;
  numberOfOperationsClose: number | null = null;
  operationsClose: IOperation[] = [];
  possibleOperationsToMark: IOperation[] = [];

  constructor(
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private eventManager: EventManager,
    private myaOperationService: MyaOperationService,
    private myaBudgetItemPeriodService: MyaBudgetItemPeriodService
  ) {}

  ngOnInit(): void {
    this.countOperationsCloseToBudgetItemPeriod();
  }

  edit(): void {
    const modalRef = this.modalService.open(MyaBudgetItemPeriodUpdateDialogComponent, { size: 'lg', backdrop: 'static', animation: true });
    modalRef.componentInstance.setBudgetItemPeriod(this.budgetItemPeriod, this.budgetItem);
  }

  new(): void {
    const modalRef = this.modalService.open(MyaBudgetItemPeriodUpdateDialogComponent, { size: 'lg', backdrop: 'static', animation: true });
    modalRef.componentInstance.setBudgetItemPeriod({ month: this.budgetItemPeriod?.month, modifyNexts: false }, this.budgetItem);
  }

  delete(): void {
    const modalRef = this.modalService.open(MyaBudgetItemPeriodDeleteDialogComponent, { backdrop: 'static', animation: true });
    modalRef.componentInstance.budgetItemPeriod = this.budgetItemPeriod;
  }

  mark(op: IOperation): void {
    this.budgetItemPeriod!.operation = op;
    this.budgetItemPeriod!.date = op.date;
    this.myaBudgetItemPeriodService.update(this.budgetItemPeriod!).subscribe();
    this.eventManager.broadcast({ name: EVENT_LOAD_BUDGET_ITEMS, content: 'OK' });
  }

  countOperationsCloseToBudgetItemPeriod(): void {
    if (
      this.budgetItemPeriod &&
      this.budgetItem &&
      this.budgetItem.category &&
      this.editable &&
      !this.budgetItemPeriod.isSmoothed &&
      this.budgetItemPeriod.amount !== 0
    ) {
      this.myaOperationService
        .findOperationsCloseToBudgetItemPeriod(this.budgetItemPeriod.amount!, this.budgetItem.category.id, this.budgetItemPeriod.date!)
        .subscribe((res: HttpResponse<IOperation[]>) => {
          this.operationsClose = res.body!;
          this.possibleOperationsToMark = new Array<IOperation>();
          this.operationsClose.forEach(o => {
            if (o.amount === this.budgetItemPeriod?.amount && o.budgetItemPeriod === null) {
              this.possibleOperationsToMark.push(o);
            }
          });
          this.numberOfOperationsClose = this.operationsClose.length;
        });
    }
  }

  dateInFuture(date: Dayjs): boolean {
    const today = dayjs();
    return today.isBefore(date);
  }
}
