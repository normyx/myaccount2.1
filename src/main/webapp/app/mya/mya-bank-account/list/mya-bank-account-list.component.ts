import { Component, OnInit } from '@angular/core';
import { EventManager } from 'app/core/util/event-manager.service';

import { BankAccountType } from 'app/entities/enumerations/bank-account-type.model';
import { EVENT_LOAD_BANK_ACCOUNTS } from 'app/mya/config/mya.event.constants';
import { Subscription } from 'rxjs';
import { IBankAccount } from '../../../entities/bank-account/bank-account.model';
import { EntityArrayResponseType } from '../../../entities/bank-account/service/bank-account.service';
import { IBankAccountTotal } from '../row/mya-bank-account-total.model';
import { MyaBankAccountService } from '../service/mya-bank-account.service';
import { IAccountGroup } from 'app/entities/account-group/account-group.model';
import { MyaAccountGroupService } from 'app/mya/mya-account-group/service/mya-account-group.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-mya-bank-account-list',
  templateUrl: './mya-bank-account-list.component.html',
})
export class MyaBankAccountListComponent implements OnInit {
  private static readonly NOT_SORTABLE_FIELDS_AFTER_SEARCH = ['accountName', 'accountBank', 'shortName', 'accountType'];

  currentAccountTotal = 0;
  savingsAccountTotal = 0;
  portfolioTotal = 0;
  realEstateTotal = 0;
  total = 0;
  withArchived = false;
  dashboardType = 'all';

  bankAccounts: IBankAccount[] | null = null;
  currentBankAccounts: IBankAccount[] | null = null;
  savingsBankAccounts: IBankAccount[] | null = null;
  portfolioBankAccounts: IBankAccount[] | null = null;
  realEstateBankAccounts: IBankAccount[] | null = null;
  accountGroups: IAccountGroup[] | null = null;

  eventSubscriber: Subscription | null = null;
  accountGroupIds: number[] = new Array<number>();

  constructor(
    protected bankAccountService: MyaBankAccountService,
    private eventManager: EventManager,
    protected accountGroupService: MyaAccountGroupService
  ) {}

  addToTotalEvent(total: IBankAccountTotal): void {
    switch (total.bankAccount.accountType) {
      case BankAccountType.CURRENTACCOUNT:
        this.currentAccountTotal += total.total;
        break;
      case BankAccountType.SAVINGSACCOUNT:
        this.savingsAccountTotal += total.total;
        break;
      case BankAccountType.STOCKPORTFOLIO:
        this.portfolioTotal += total.total;
        break;
      case BankAccountType.REAL_ESTATE:
        this.realEstateTotal += total.total;
        break;
    }
    this.total += total.total;
  }

  changeDashboardType(type: string): void {
    this.dashboardType = type;
  }

  ngOnInit(): void {
    this.eventSubscriber = this.eventManager.subscribe(EVENT_LOAD_BANK_ACCOUNTS, () => this.load());
    this.load();
  }

  load(): void {
    this.currentAccountTotal = 0;
    this.savingsAccountTotal = 0;
    this.portfolioTotal = 0;
    this.total = 0;
    this.accountGroupService.queryWithSignedInUser().subscribe((res: HttpResponse<IAccountGroup[]>) => {
      if (this.accountGroups == null) {
        this.accountGroups = res.body;
      }
      this.accountGroupIds = new Array<number>();
      this.accountGroups?.forEach(ag => {
        if (ag.defaultActive) {
          this.accountGroupIds.push(ag.id);
        }
      });
      this.bankAccountService.queryWithAccountGroups(this.accountGroupIds).subscribe((res2: EntityArrayResponseType) => {
        this.bankAccounts = res2.body;
        if (this.bankAccounts) {
          this.currentBankAccounts = this.sortBankAccounts(
            this.bankAccounts.filter(ba => ba.accountType === BankAccountType.CURRENTACCOUNT)
          );

          this.savingsBankAccounts = this.sortBankAccounts(
            this.bankAccounts.filter(ba => ba.accountType === BankAccountType.SAVINGSACCOUNT)
          );
          this.portfolioBankAccounts = this.sortBankAccounts(
            this.bankAccounts.filter(ba => ba.accountType === BankAccountType.STOCKPORTFOLIO)
          );
          this.realEstateBankAccounts = this.sortBankAccounts(
            this.bankAccounts.filter(ba => ba.accountType === BankAccountType.REAL_ESTATE)
          );
        }
      });
    });
  }

  protected changeAccountGroup(event: any): void {
    this.load();
  }

  private sortBankAccounts(bankAccount: IBankAccount[]): IBankAccount[] {
    return bankAccount.sort((a: IBankAccount, b: IBankAccount) => {
      if (a.archived === b.archived) {
        if (a.accountBank === b.accountBank) {
          return a.accountName! < b.accountName! ? 1 : -1;
        } else {
          return a.accountBank! < b.accountBank! ? 1 : -1;
        }
      } else {
        return a.archived ? 1 : -1;
      }
    });
  }
}
