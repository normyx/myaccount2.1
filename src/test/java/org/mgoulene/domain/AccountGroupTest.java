package org.mgoulene.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.mgoulene.web.rest.TestUtil;

class AccountGroupTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountGroup.class);
        AccountGroup accountGroup1 = new AccountGroup();
        accountGroup1.setId(1L);
        AccountGroup accountGroup2 = new AccountGroup();
        accountGroup2.setId(accountGroup1.getId());
        assertThat(accountGroup1).isEqualTo(accountGroup2);
        accountGroup2.setId(2L);
        assertThat(accountGroup1).isNotEqualTo(accountGroup2);
        accountGroup1.setId(null);
        assertThat(accountGroup1).isNotEqualTo(accountGroup2);
    }
}
